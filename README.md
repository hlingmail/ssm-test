# README #
 
This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
User need to list the content of the root directory of all servers queried by aws tag without need to ssh to the servers individually.
This repository includes the soluttion for this purpose.
### Content of the repos ###
1. SSM-PrintRoot.yaml: this is a cloudformation template that will be used to deploy lambda function which will return the list of the ec2 instance by tag
2. SSM-document.yaml: this is the custom ssm automate document to be run to print out the content of the selected ec2 filtered by tag. 

### How do I get set up? ###
1. Make sure SSM can communicate with the linuxed servers by configure and Install the SSM Agent in the Linux server.
2. use AWS Cloudformation service to load the cloudformation template SSM-PrintRoot.yaml
3. run the following aws cli to create the ssm document: 

aws ssm create-document --content file:///{{home directory}}/SSM-document.yaml  --name "PrintRoot" --document-type "Automation" --document-format YAML


